﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialStage : MonoBehaviour
{
    float timer = 2;

    public Health Janus;

    public MoveToPosition lavaLeft;

    public MoveToPosition lavaRight;

    public MoveToPosition lavaFront;

    private void Start()
    {
        lavaLeft.enabled = false;
        lavaRight.enabled = false;
        lavaFront.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        if(Janus.getHealth() < Janus.maxHealth * 0.65f)
        {
            timer -= Time.deltaTime;
            lavaLeft.enabled = true;
            lavaRight.enabled = true;
            lavaFront.enabled = true;

        }
        if (timer < 0)
        {
            timer = 2;
            GetComponent<MaterialFlasher>().flash("Warning", 1f);
        }
    }
}
