﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealthBasedToastCaller : MonoBehaviour
{
    public string[] instructionList = {
        "Use LEFT STICK to MOVE",
        "Use RIGHT STICK to AIM and \n RIGHT TRIGGER to SHOOT",
        "Use low ground to avoid attacks",
        "Use LEFT TRIGGER to DASH",
        "Get to high ground!",
        "DASHING makes you INVULNERABLE \n Use it to dodge through attacks",
        "Use either BUMPER to use the SPECIAL",
        "Go get him!",
        "Step on the BLUE plate to fight Pluto (easy) \n Step on the RED plate to fight Mars (harder)"
    };
    public float[] instructionPercentages =
    {
        1.1f,
        1.1f,
        .90f,
        .75f,
        .65f,
        .55f,
        .4f,
        .3f,
        0f
    };
    [SerializeField] int index = 0;

    public GameObject boss;
    private Health bossHealth;
    // Start is called before the first frame update

    private void Start()
    {
        bossHealth = boss.GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        if ( index < instructionList.Length && ((float)bossHealth.getHealth() / (float)bossHealth.maxHealth) <= instructionPercentages[index])
        {
            Debug.Log((float)bossHealth.getHealth() / (float)bossHealth.maxHealth);
            ToastManager.Toast(instructionList[index]);
            ++index;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            bossHealth.takeDamage(1);
        }
    }
}
