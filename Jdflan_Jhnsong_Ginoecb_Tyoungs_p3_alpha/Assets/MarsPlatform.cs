﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarsPlatform : MonoBehaviour
{
    public Health Mars;

    public float time;

    private float timer;

    private bool activated;

    public float moveDistance;

    private void Start()
    {
        timer = 0;
        activated = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Mars.getHealth() < Mars.maxHealth * 0.20f && !activated)
        {
            timer = time;
            activated = true;
        }
        if(timer > 0)
        {
            transform.RotateAroundLocal(Vector3.up, (360 / time) * Time.deltaTime * Mathf.Deg2Rad);
            transform.position += Vector3.up * (moveDistance / time) * Time.deltaTime;
            timer -= Time.deltaTime;
        }
    }
}
