﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AoEHealing : AoEBase
{
    [SerializeField] private int healing;
    
    protected override void OnTriggerEffect(GameObject other)
    {
        // Added check for player tag to prevent healing shields
        if (other.gameObject.CompareTag("Player") && other.GetComponent<Health>() != null && other.GetComponent<Health>().isPlayer)
            other.GetComponent<Health>().takeDamage(-1 * healing);

        if (other.gameObject.CompareTag("Player") && other.GetComponentInChildren<Health>() != null && other.GetComponentInChildren<Health>().isPlayer)
            other.GetComponentInChildren<Health>().takeDamage(-1 * healing);
    }
}
