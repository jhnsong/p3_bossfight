﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class MultiPlayerRespawner : MonoBehaviour
{
    [SerializeField] public float respawnTime = 5;
    public static MultiPlayerRespawner instance;
    public int countDead = 0;
    public int respawnLoc = 0;
    [SerializeField] public GameObject[] tutorialRespawnPoints;
    private List<Gamepad> pads;
    public bool shouldGoToGameOver = false;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
        pads = new List<Gamepad>(Gamepad.all);
    }

    // Update is called once per frame
    void Update()
    {
        if (countDead >= GameController.Instance.Players.Length && (SceneManager.GetActiveScene().name != CharacterSelector.instance.menuSceneName && SceneManager.GetActiveScene().name != CharacterSelector.instance.gameoverSceneName)
            && SceneManager.GetActiveScene().name != CharacterSelector.instance.winSceneName)
        {
            Debug.Log(countDead);
            GameOver();
        }
    }

    private IEnumerator DoRespawn(GameObject player)
    {
        //vibrate controller
        GameController.Instance.VibrateSpecificController(GetPlayerNumber(player), 1f, 0.5f);
        if (SceneManager.GetActiveScene().name == CharacterSelector.instance.menuSceneName)
        {
            yield return null;
            GameController.Instance.Players[GetPlayerIndex(player)].GetComponentInChildren<Health>().reset();
            player.transform.position = CharacterSelector.instance.GetSpawnLoc();
			player.GetComponent<Health>().setInvincible(5);
			player.GetComponent<Rigidbody>().velocity = Vector3.zero;
			GameObject newRespwan = Instantiate(player.GetComponent<Health>().respawn);
			newRespwan.transform.position = player.transform.position;
		}
        else
        {
           // Debug.Log(GetPlayerIndex(player));
            countDead += 1;
            //Debug.Log(player.name);

            //var thing = player.GetComponentInChildren<Health>().gameObject;
            player.SetActive(false);
            yield return new WaitForSeconds(respawnTime);
            player.SetActive(true);
            GameController.Instance.Players[GetPlayerIndex(player)].SetActive(true);
            Debug.Log(0);
            GameController.Instance.Players[GetPlayerIndex(player)].GetComponent<Health>().reset();
            player.transform.position = CharacterSelector.instance.GetSpawnLoc();
            player.GetComponent<Health>().setInvincible(5);
            player.GetComponent<Rigidbody>().velocity = Vector3.zero;
			GameObject newRespwan = Instantiate(player.GetComponent<Health>().respawn);
			newRespwan.transform.position = player.transform.position;
            //vibrate controller
            GameController.Instance.VibrateSpecificController(GetPlayerNumber(player), 0.5f, 0.75f);

            countDead -= 1;
        }
    }

    public void Respawn(GameObject player)
    {
        StartCoroutine(DoRespawn(player));
    }

    private int GetPlayerIndex(GameObject gameObject)
    {
        if (gameObject.CompareTag("Player"))
        {
            return GameController.Instance.GetPlayerIndex(gameObject);
            //  return (gameObject.GetComponent<PlayerController>().playerNum - 1);
        }
        // Please dont do this
        Debug.Log("returning -1 from multi");
        return -1;
    }

    private int GetPlayerNumber(GameObject play)
    {
        return play.GetComponent<PlayerController>().playerNum;
    }

    void GameOver()
    {
        StartCoroutine(DoGameOver());
       // SceneManager.LoadScene(CharacterSelector.instance.gameoverSceneName);
       // StartCoroutine(CharacterSelector.instance.TransitionIn(CharacterSelector.instance.gameoverSceneName));
       // SceneManager.LoadScene(CharacterSelector.instance.gameoverSceneName);
    }

    private IEnumerator DoGameOver()
    {
        Debug.Log("Doing game over");
        // StartCoroutine(CharacterSelector.instance.GG());
        CharacterSelector.instance.GameOver();
        countDead = 0;
        CharacterSelector.instance.playersHaveSpawned = true;
        yield return new WaitForSeconds(1.2f);
        CharacterSelector.instance.playersHaveSpawned = false;
        SceneManager.LoadScene(CharacterSelector.instance.gameoverSceneName);
    }
}
