﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanningCamera : MonoBehaviour
{

    public float speed;

    public bool vertical;

    // Update is called once per frame
    void Update()
    {
        if (!vertical)
        {
            transform.localEulerAngles += Vector3.up * speed * Time.deltaTime;
        } else
        {
            transform.localEulerAngles += Vector3.right * speed * Time.deltaTime;
        }
    }
}
