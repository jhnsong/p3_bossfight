﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JanusDeathAnim : MonoBehaviour
{
	public GameObject explosion;
	public GameObject explosionFinal;
	private bool boom;
	private bool boomFinal;

	private float animTime = 1f;
	private float animTimer;

	void Start()
    {
		boom = true;
		boomFinal = true;

		animTimer = animTime;
	}

    // Update is called once per frame
    void Update()
    {
		if (GetComponent<Health>().getHealth() <= 5 && GetComponent<Health>().getHealth() >= -5)
		{
			GetComponent<Health>().setInvincible(999.0f);
			GetComponent<Health>().bossDeath = true;
			GetComponentInChildren<WeaponsHolder>().PauseAll();
			GetComponent<miniBoss>().enabled = false;

			foreach (Transform child in transform) {
				if (child.CompareTag("JS0") || child.CompareTag("JS1") || child.CompareTag("JS2"))
				{
					child.GetComponent<MeshRenderer>().enabled = false;
				}
			}

			if (boom)
			{
				GameObject newExplosion = Instantiate(explosion);
				newExplosion.transform.position = transform.position;
				boom = false;
			}

			if (animTimer <= 0)
			{
				if (boomFinal)
				{
					GameObject newExplosion = Instantiate(explosionFinal);
					newExplosion.transform.position = transform.position;
					boomFinal = false;
				}
				GetComponent<Health>().takeDamage(1);
			}
			animTimer -= Time.deltaTime;
		}
    }
}
