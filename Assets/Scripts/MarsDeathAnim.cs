﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarsDeathAnim : MonoBehaviour
{
	private float flashTimeMax = 1.5f;
	private float flashTime;
	private float flashTimer;
	private float explosionTimeMax = 1.5f;
	private float explosionTime;
	private float explosionTimer;
	private float animTime = 10f;
	private float animTimer;
	private bool reset;

	private SpiderLeg[] legs;

	public GameObject explosion;
	public GameObject explosionFinal;
	// Start is called before the first frame update
	void Start()
	{
		flashTime = flashTimeMax;
		flashTimer = 0f;
		explosionTime = explosionTimeMax;
		explosionTimer = 0f;
		animTimer = animTime;

		legs = GetComponentsInChildren<SpiderLeg>();
		reset = true;

	}

	// Update is called once per frame
	void Update()
	{
		if (GetComponent<Health>().getHealth() <= 5)
		{
			GetComponent<Health>().setInvincible(999.0f);
			GetComponent<Health>().bossDeath = true;
			GetComponentInChildren<WeaponsHolder>().PauseAll();
			GetComponent<SpiderBody>().enabled = false;

			if (reset)
			{
				foreach (SpiderLeg leg in legs)
				{
					leg.Reset();
					leg.GetComponent<Health>().setInvincible(999.0f);
					leg.GetComponent<Health>().bossDeath = true;
					leg.GetComponentInChildren<Canvas>().enabled = false;
				}
				transform.position += Vector3.up * 3;
				reset = false;
			}

			if (GetComponent<MaterialFlasher>() && flashTimer <= 0f)
			{
				GetComponent<MaterialFlasher>().flash("Hurt", 0.05f);
				foreach (SpiderLeg leg in legs)
				{
					leg.GetComponent<MaterialFlasher>().flash("Hurt", 0.05f);
				}
				flashTimer = flashTime;
			}
			flashTimer -= Time.deltaTime;

			if (explosionTimer <= 0f)
			{
				GameObject newExplosion = Instantiate(explosion);
				newExplosion.transform.position = new Vector3(Random.Range(transform.position.x - 10f, transform.position.x + 10f),
															  Random.Range(transform.position.y + 0f, transform.position.y + 0f),
															  Random.Range(transform.position.z - 10f, transform.position.z + 10f));
				explosionTimer = explosionTime;
			}
			explosionTimer -= Time.deltaTime;

			bool boom = true;
			if (animTimer <= 0)
			{
				if (boom)
				{
					GameObject newExplosion = Instantiate(explosionFinal);
					newExplosion.transform.position = transform.position;
					boom = false;
				}
				GetComponent<Health>().takeDamage(1);
			}
			animTimer -= Time.deltaTime;
			explosionTime = explosionTimeMax * (animTimer / animTime);
			flashTime = flashTimeMax * (animTimer / animTime);
		}
	}
}
