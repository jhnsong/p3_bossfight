﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPulse : MonoBehaviour
{
    [SerializeField] private float pulseSpeed;
    [SerializeField] private bool isRadial;
    [Tooltip("Pulse occurs vertically if no selection is made")]
    [SerializeField] private bool isHorizontal;

    void Update()
    {
        float newScale = 1.1f + Mathf.Sin(pulseSpeed * Time.time) * 0.15f;
        if (isHorizontal)
        {
            this.transform.localScale = new Vector3(newScale, 1, 1);
            return;
        }
        this.transform.localScale = new Vector3(isRadial ? newScale : 1, newScale, 1);
    }
}
