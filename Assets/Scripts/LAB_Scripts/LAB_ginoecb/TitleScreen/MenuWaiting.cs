﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuWaiting : MonoBehaviour
{
    [SerializeField] private Text text;
    private int numPeriods = 0;
    private int numPeriodsMax = 4;
    [Tooltip("Update delay should never be less than or equal to zero")]
    [SerializeField] private float updateDelay = 0.25f;
    private bool canUpdate;
    [SerializeField] private List<MenuRegisterPlayer> players;

    [SerializeField] private GameObject StartPrompt;

    private void Start()
    {
        text = this.GetComponent<Text>();
        canUpdate = true;
    }

    private void Update()
    {
        CheckForRegistered();
        UpdateText();
    }

    private void UpdateText()
    {
        if (canUpdate)
        {
            text.text = "Waiting for Players ";

            // Add periods to text
            for (int i = 0; i < numPeriods; i++)
            {
                text.text += ".";
            }

            // Increment number of periods
            numPeriods = numPeriods + 1 < numPeriodsMax + 1 ? numPeriods + 1 : 0;

            // Report update
            StartCoroutine(OnUpdate());
        }
    }

    // Disables self if a registered player is detected
    private void CheckForRegistered()
    {
        foreach (MenuRegisterPlayer player in players)
        {
            if (player.isRegistered)
            {
                StartPrompt.SetActive(true);
                this.gameObject.SetActive(false);
            }
        }
    }

    private IEnumerator OnUpdate()
    {
        canUpdate = false;
        yield return new WaitForSeconds(updateDelay);
        canUpdate = true;
    }
}
