﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuRegisterPlayer : MonoBehaviour
{
    public bool isRegistered = false;
    public MenuManager manager;

    [Header("Unregistered")]
    [SerializeField] private GameObject unregistered;
    [Header("Registered")]
    [SerializeField] private GameObject registered;
    [Header("Images")]
    [SerializeField] private GameObject healerImg;
    [SerializeField] private GameObject shieldImg;
    [SerializeField] private GameObject attackImg;
    [Header("Text")]
    [SerializeField] private Text playerNumber;
    [SerializeField] private Text abilityName;
    [Header("Other")]
    public ControllerInput input;
    [Tooltip("Players are numbered 1 to 3, not 0 to 2")]
    [SerializeField] private int num = 1;

    private void Awake()
    {
        input = this.gameObject.AddComponent<ControllerInput>();
        input.OnAwake(num);
        manager = FindObjectOfType<MenuManager>();
    }

    private void Update()
    {
        if (isRegistered == false && input.A)
        {
            OnRegister();
        }
    }

    private void OnRegister()
    {
        var sfx = FindObjectOfType<AudioSource>();
        sfx.Stop();
        sfx.Play();

        // Change visible canvas components
        unregistered.SetActive(false);
        registered.SetActive(true);

        // Set image and player name
        switch (num)
        {
            // Healer
            case 1:
                OnRegisterPlayerUI(this.healerImg, "Healer");
                break;
            
            // Shield
            case 2:
                OnRegisterPlayerUI(this.shieldImg, "Shield");
                break;

            // Attack Up
            case 3:
                OnRegisterPlayerUI(this.attackImg, "Attack+");
                break;

            // Error handlng
            default:
                Debug.LogError("Invalid player number on main menu registration");
                break;
        }

        // Add player to list of active players
        manager.activePlayers.Add(num);

        // TODO: Add controller vibration


        isRegistered = true;
    }

    private void OnRegisterPlayerUI(GameObject img, string str)
    {
        img.SetActive(true);
        this.playerNumber.text = "Player " + this.num.ToString() + " Joined as";
        this.abilityName.text = str;
    }
}
