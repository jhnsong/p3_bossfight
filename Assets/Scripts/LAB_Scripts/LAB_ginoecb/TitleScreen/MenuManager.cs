﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [Header("Text")]
    [SerializeField] private GameObject StartPrompt;
    [SerializeField] private GameObject SoloPrompt;
    [Header("Players")]
    [SerializeField] private List<MenuRegisterPlayer> players;
    public List<int> activePlayers;

    public static MenuManager instance;

    private void Awake()
    {
        // Singleton, but always update to the newest version
        if (instance != null)
        {
            Destroy(instance.gameObject);
        }
        instance = this;

        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        // Press Start to begin
        if (StartPrompt != null && StartPrompt.activeSelf)
        {
            foreach (MenuRegisterPlayer player in players)
            {
                if (player.input.StartButton)
                {
                    var sfx = FindObjectOfType<AudioSource>();
                    sfx.Stop();
                    sfx.Play();

                    // Load SoloPrompt if necessary
                    if (activePlayers.Count == 1)
                    {
                        StartPrompt.SetActive(false);
                        SoloPrompt.SetActive(true);
                    }
                    // Load tutorial otherwise
                    else
                    {
                        LoadTutorialScene();
                    }
                }
            }
        }
        // If only 1 player is registered & Start is pressed
        else if (SoloPrompt != null && SoloPrompt.activeSelf)
        {
            foreach (MenuRegisterPlayer player in players)
            {
                // Confirm
                if (player.input.X)
                {
                    var sfx = FindObjectOfType<AudioSource>();
                    sfx.Stop();
                    sfx.Play();

                    LoadTutorialScene();
                }
                // Cancel
                else if (player.input.B)
                {
                    var sfx = FindObjectOfType<AudioSource>();
                    sfx.Stop();
                    sfx.Play();

                    StartPrompt.SetActive(true);
                    SoloPrompt.SetActive(false);
                }
            }
        }
    }

    // Returns a list of all active players
    // and destroys the menu manager
    public List<int> GetActivePlayers()
    {
        // Arbitrarily long length
        //StartCoroutine(DestroyAfterSeconds(10));
        return activePlayers;
    }

    #region Helpers

    private void LoadTutorialScene()
    {
        SceneManager.LoadScene("tutorial_lab");
        // TODO: Replace with method to load tutorial scene with transition
    }

    private IEnumerator DestroyAfterSeconds(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);
    }

    #endregion Helpers
}
