﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MenuInput : MonoBehaviour
{
    private List<Gamepad> gamepads;

    private void Update()
    {
        gamepads = new List<Gamepad>(Gamepad.all);
    }

    public bool PressedA()
    {
        if (gamepads == null) return false;
        foreach (Gamepad gamepad in gamepads)
            if (gamepad.aButton.wasPressedThisFrame) { return true; }

        return false;
    }

    public bool PressedStart()
    {
        if (gamepads == null) return false;
        foreach (Gamepad gamepad in gamepads)
        {
            if (gamepad.startButton.wasPressedThisFrame) return true;
        }
        return false;
    }

    public bool PressedB()
    {
        if (gamepads == null) return false;
        foreach (Gamepad gamepad in gamepads)
            if (gamepad != null && gamepad.bButton != null && gamepad.bButton.wasPressedThisFrame) { return true; }

        return false;
    }

    public bool PressedX()
    {
        if (gamepads == null) return false;
        foreach (Gamepad gamepad in gamepads)
            if (gamepad != null && gamepad.xButton != null && gamepad.xButton.wasPressedThisFrame) { return true; }

        return false;
    }
}
