﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderBody : MonoBehaviour
{

    private SpiderLeg[] legs;

    public float downTime;
    private float downTimer;

    public float dropDistance;

    private float waitTimer;

    // Start is called before the first frame update
    void Start()
    {
        legs = GetComponentsInChildren<SpiderLeg>();
        downTimer = 0;
        waitTimer = 7;
        GetComponentInChildren<WeaponsHolder>().PauseAll();
    }

    // Update is called once per frame
    void Update()
    {
        if (waitTimer > 0)
        {
            waitTimer -= Time.deltaTime;
            if (waitTimer <= 0)
            {
                GetComponentInChildren<WeaponsHolder>().ResumeAll();
            }
            return;
        }
        if (downTimer > 0)
        {
            downTimer -= Time.deltaTime;
            if(downTimer <= 0)
            {
                foreach (SpiderLeg leg in legs)
                {
                    leg.Reset();
                    GetComponentInChildren<WeaponsHolder>().ResumeAll();
                }
                transform.position += Vector3.up * dropDistance;
            }
        } else
        {
            bool legAlive = false;
            foreach (SpiderLeg leg in legs)
            {
                if (!leg.isDead())
                {
                    legAlive = true;
                }
            }
            if (!legAlive)
            {
                downTimer = downTime;
                transform.position += Vector3.down * dropDistance;
                if (GetComponentInChildren<WeaponsHolder>())
                {
                    GetComponentInChildren<WeaponsHolder>().PauseAll();
                }
            }
        }
    }

    public void forceReset(float timer)
    {
        if(downTimer > 0)
        {
            downTimer = 0;
            foreach (SpiderLeg leg in legs)
            {
                leg.Reset();
                GetComponentInChildren<WeaponsHolder>().ResumeAll();
            }
            transform.position += Vector3.up * dropDistance;
        }
        GetComponentInChildren<WeaponsHolder>().PauseAll();
        waitTimer = timer;
    }
}
