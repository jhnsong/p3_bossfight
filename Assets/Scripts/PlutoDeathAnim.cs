﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlutoDeathAnim : MonoBehaviour
{
	private float flashTimeMax = 2f;
	private float flashTime;
	private float flashTimer;
	private float explosionTimeMax = 2f;
	private float explosionTime;
	private float explosionTimer;
	private WeaponsHolder weapons;
	private float animTime = 10f;
	private float animTimer;

	public GameObject explosion;
	public GameObject explosionFinal;
	// Start is called before the first frame update
	void Start()
    {
		flashTime = flashTimeMax;
		flashTimer = 1f;
		explosionTime = explosionTimeMax;
		explosionTimer = 1f;
		weapons = GetComponentInChildren<WeaponsHolder>();
		animTimer = animTime;
	}

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Health>().getHealth() <= 5)
		{
			GetComponent<Health>().setInvincible(999.0f);
			GetComponent<Health>().bossDeath = true;
			weapons.PauseAll();
			GetComponent<BurrowMovement>().enabled = false;
			

			if (GetComponent<MaterialFlasher>() && flashTimer <= 0f)
			{
				GetComponent<MaterialFlasher>().flash("Hurt", 0.05f);
				flashTimer = flashTime;
			}
			flashTimer -= Time.deltaTime;

			if (explosionTimer <= 0f)
			{
				GameObject newExplosion = Instantiate(explosion);
				newExplosion.transform.position = new Vector3(Random.Range(transform.position.x - 3f, transform.position.x + 3f),
															  Random.Range(transform.position.y + 0f, transform.position.y + 7f),
															  Random.Range(transform.position.z - 5f, transform.position.z + 0f));
				explosionTimer = explosionTime;
			}
			explosionTimer -= Time.deltaTime;

			bool boom = true;
			if (animTimer <= 0)
			{
				if (boom)
				{
					GameObject newExplosion = Instantiate(explosionFinal);
					newExplosion.transform.position = new Vector3(transform.position.x, transform.position.y + 3, transform.position.z);
					boom = false;
				}
				GetComponent<Health>().takeDamage(1);
			}
			animTimer -= Time.deltaTime;
			explosionTime = explosionTimeMax * (animTimer / animTime);
			flashTime = flashTimeMax * (animTimer / animTime);
		}
	}
}
