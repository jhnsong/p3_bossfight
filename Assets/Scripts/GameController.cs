﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class GameController : MonoBehaviour
{
    
    public GameObject[] Players;
    public string loadedLevel = "";
    public List<Gamepad> pads;
   // public List<GameObject> PlayerHealth;

    public Canvas canvas;

    public GameObject winScreen;

    public GameObject loseScreen;

    public Health boss;

    public static GameController Instance;

    private void Awake()
    {
        Debug.Log("AWAKE");
        if (GameController.Instance)
        {
            Destroy(gameObject);
        } else
        {
            GameController.Instance = this;
        }
        pads = new List<Gamepad>(Gamepad.all);

        MenuManager menuManager = FindObjectOfType<MenuManager>();
        if (menuManager != null)
        {
            var activePlayers = menuManager.GetActivePlayers();

            Players = new GameObject[activePlayers.Count];

            // List of active players from title screen
            // Controller number(s) are provided
            foreach (int num in activePlayers)
            {
                // TODO: Add players to Players[]   
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
      //  winScreen.active = false;
      //  loseScreen.active = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            Time.timeScale = 1;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
        if (!boss && GameObject.FindGameObjectWithTag("Boss"))
        {
            boss = GameObject.FindGameObjectWithTag("Boss").GetComponent<Health>();
        }
        if ( SceneManager.GetActiveScene().name != CharacterSelector.instance.winSceneName && SceneManager.GetActiveScene().name != CharacterSelector.instance.menuSceneName 
            && SceneManager.GetActiveScene().name != CharacterSelector.instance.gameoverSceneName && boss.getHealth() <= 0)
        {
            if (SceneManager.GetActiveScene().name != CharacterSelector.instance.firstLevelName)
            {
                Debug.Log("YOU WON THE GAME");
                CharacterSelector.instance.GameOver();
                //StartCoroutine(CharacterSelector.instance.TransitionIn(CharacterSelector.instance.winSceneName));
                SceneManager.LoadScene(CharacterSelector.instance.winSceneName);
            }
            else
            {
                if (boss.gameObject.activeSelf)
                {
                    CharacterSelector.instance.PlutoWin();
                    boss.gameObject.SetActive(false);
                }
            }
        }
        /*
        int playerTotalHealth = 0;
        foreach(GameObject player in Players)
        {
            if(player.GetComponentInChildren<Health>().getHealth() > 0)
            playerTotalHealth += player.GetComponentInChildren<Health>().getHealth();
        }
        if(playerTotalHealth <= 0 && !winScreen.active)
        {
            loseScreen.active = true;
        }
        if (boss.getHealth() <= 0 && !loseScreen.active)
        {
            winScreen.active = true;
        }
        if(Input.GetButton("Submit") && (winScreen.active || loseScreen.active))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }*/
    }

    public void AddPlayer(int index, GameObject player)
    {
        int i = 0;
        for (; i < Players.Length; ++i)
        {
            if (Players[i] == null)
            {
                break;
            }
            else if (Players[i].GetComponentInChildren<PlayerController>().playerNum == player.GetComponentInChildren<PlayerController>().playerNum) {
                Players[i] = player;
                return;
            }
        }
        Debug.Log("index is " + i);
        Debug.Log(player.GetComponentInChildren<PlayerController>().playerNum);
        Players[i] = player;
    }

    public void VibrateAllControllers(float duration, float frequency=0.5f)
    {
        if (frequency >= 1 || frequency <= 0)
        {
            frequency = 0.5f;
        }
        StartCoroutine(VibrateAllControllersCoroutine(duration, frequency));
    }

    private IEnumerator VibrateAllControllersCoroutine(float duration, float frequency)
    {
        for (int i = 0; i < pads.Count; ++i)
        {
            pads[i].SetMotorSpeeds(frequency, frequency);
            pads[i].ResumeHaptics();
        }
        yield return new WaitForSeconds(duration * Time.timeScale);
        for (int i = 0; i < pads.Count; ++i)
        {
            pads[i].SetMotorSpeeds(0f, 0f);
            pads[i].PauseHaptics();
        }
    }

    public void VibrateSpecificController(int playerNum, float duration, float frequency = 0.5f)
    {
        if (frequency >= 1 || frequency <= 0)
        {
            frequency = 0.5f;
        }
        StartCoroutine(VibrateSpecificControllerCoroutine(playerNum, duration, frequency));
    }


    private IEnumerator VibrateSpecificControllerCoroutine(int playerNum, float duration, float frequency)
    {
        pads[playerNum - 1].SetMotorSpeeds(frequency, frequency);
        pads[playerNum - 1].ResumeHaptics();
        yield return new WaitForSeconds(duration * Time.timeScale);
        pads[playerNum - 1].SetMotorSpeeds(0f, 0f);
        pads[playerNum - 1].PauseHaptics();
    }

    public int GetPlayerIndex(GameObject player)
    {
        for (int i = 0; i < Players.Length; ++i)
        {
            if (Players[i].GetComponent<PlayerController>().playerNum == player.GetComponent<PlayerController>().playerNum)
            {
                return i;
            }
        }
        Debug.Log("Returning -1 from game control");
        return -1;
    }

    public int GetPlayerIndex(int playerNum)
    {
        for (int i = 0; i < Players.Length; ++i)
        {
            if (Players[i].GetComponent<PlayerController>().playerNum == playerNum)
            {
                return i;
            }
        }
        return -1;
    }
}
