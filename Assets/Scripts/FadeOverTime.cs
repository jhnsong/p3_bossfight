﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOverTime : MonoBehaviour
{
	[SerializeField] float delay;
	[SerializeField] float fadeMag;
	private Color color;
	private float fadeTimer;

	void Start()
    {
		color = GetComponent<MeshRenderer>().material.color;
    }

    void Update()
    {
		delay -= Time.deltaTime;
		if (delay < 0 && color.a > 0)
		{
			color.a -= fadeMag * Time.deltaTime;
			GetComponent<MeshRenderer>().material.color = color;
		}
	}
}
