﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathCamera : MonoBehaviour
{
    public float time;
    private float timer;
    private Transform originalParent;
    private Vector3 offset;

    private void Start()
    {
        timer = time;
        originalParent = transform.parent;
        offset = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            Time.timeScale = 1;
            timer = time;
            gameObject.active = false;
            transform.parent = originalParent;
            transform.localPosition = offset;
        }
    }
}
