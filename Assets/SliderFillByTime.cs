﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderFillByTime : MonoBehaviour
{
    public Slider slide;
    // Start is called before the first frame update
    void Awake()
    {
        slide = GetComponent<Slider>();
    }

    private void OnEnable()
    {
        slide.value = 0;
        StartCoroutine(FillBar());
    }

    private IEnumerator FillBar()
    {
        Debug.Log("FILL BAR STARTED");
        float timer = 0;
        while (timer < CharacterSelector.instance.timeToFill)
        {
            timer += Time.unscaledDeltaTime;
            slide.value = timer / CharacterSelector.instance.timeToFill;
            yield return null;
        }
    }
}
