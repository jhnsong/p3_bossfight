﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelector : MonoBehaviour
{
    public Canvas theCanvas;

    public GameObject plutoWin;
    public GameObject plutoNext;

    public RectTransform transitioner;
    private Vector3 hiddenPos = new Vector3(1500, 850, 0);
    private Vector3 visible = new Vector3(200, 50, 0);
    public AnimationCurve animIn;
    public AnimationCurve animOut;
    public GameObject tutorialLoad;
    public GameObject plutoLoad;
    public GameObject marsLoad;
    public float timeToFill = 7f;

    #region Scene Management
    public string menuSceneName = "Rough_Menu";
    public string firstLevelName = "LAB_multipleplayers";
    public string secondLevelName = "";
    public string gameoverSceneName = "GameOver";
    public string winSceneName = "";
    #endregion
    public bool shouldSpawnPlayers = false;
    public bool playersHaveSpawned = false;
    private bool canvasSet = false;
    [SerializeField] private bool gameOver = false;
    public GameObject tutorialSpawnLocation;
    public GameObject level1SpawnLoc;
    public GameObject level2SpawnLoc;
    public static CharacterSelector instance;
    #region prefabs
    public GameObject shieldPrefab;
    public GameObject beamPrefab;
    public GameObject bubbleHealPrefab;
    public GameObject speedPrefab;
    public GameObject basePlayerPrefab;
    #endregion

    #region sliders
    public Slider player1Health;
    public Slider player1Special;
    public Slider player2Health;
    public Slider player2Special;
    public Slider player3Health;
    public Slider player3Special;
    public Slider player4Health;
    public Slider player4Special;
    #endregion

    #region players
    public GameObject player1;
    private bool p1Spawned = false;
    public GameObject player2;
    private bool p2Spawned = false;
    public GameObject player3;
    private bool p3Spawned = false;
    public GameObject player4;
    private bool p4Spawned = false;
    #endregion

    public int[] selections = { -1, -1, -1, -1 };
    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (instance != this && instance != null)
        {
            Destroy(gameObject);
        }
        if (instance == null)
        {
            instance = this;
        }
        MenuManager menuManager = FindObjectOfType<MenuManager>();
        if (menuManager != null)
        {
            var activePlayers = menuManager.GetActivePlayers();

           // Players = new GameObject[activePlayers.Count];

            // List of active players from title screen
            // Controller number(s) are provided
            /*foreach (int num in activePlayers)
            {
               // if (num)  
            }*/
            for (int i = 0; i < activePlayers.Count; ++i)
            {
                if (activePlayers[i] == 1)
                {
                    selections[i] = 3;
                }
                else if (activePlayers[i] == 2)
                {
                    selections[i] = 0;
                }
                else if (activePlayers[i] == 3)
                {
                    selections[i] = 2;
                }
            }
        }

    }
    private void Start()
    {
        MenuManager menuManager = FindObjectOfType<MenuManager>();
        if (menuManager)
        {
            Destroy(menuManager.gameObject);
        }
        SceneManager.sceneLoaded += OnSceneLoad;
    }

    private void OnDestroy()
    {
        Time.timeScale = 1;
        SceneManager.sceneLoaded -= OnSceneLoad;
    }

    // Update is called once per frame
    void Update()
    {
        if (!canvasSet && (SceneManager.GetActiveScene().name != menuSceneName && SceneManager.GetActiveScene().name != gameoverSceneName && SceneManager.GetActiveScene().name != winSceneName))
        {
          //  Debug.Break();
           // theCanvas.gameObject.SetActive(true);
            canvasSet = true;
        }
        if (shouldSpawnPlayers)
        {
            if (SceneManager.GetActiveScene().name == menuSceneName)
            {
                playersHaveSpawned = false;
                SpawnAllPlayers();
                shouldSpawnPlayers = false;
                return;
            }
        }
        if (((SceneManager.GetActiveScene().name != menuSceneName && SceneManager.GetActiveScene().name != gameoverSceneName) && !playersHaveSpawned && !gameOver && SceneManager.GetActiveScene().name != winSceneName))
        {
           //theCanvas.gameObject.SetActive(true);
            SpawnAllPlayers();
           // Debug.Log("PLAYERS HAVE SPAWNED TRUE");
            playersHaveSpawned = true;
        }
    }

    public void SpawnPlayer(int playerNum)
    {
        if (playersHaveSpawned)
        {
            Debug.Log("players have spawned");
            return;
        }
        GameObject prefab = GetPrefab(selections[playerNum - 1]);
        int newPlayerNum = prefab.GetComponent<PlayerController>().playerNum;
        if (newPlayerNum == 1)
        {
            player1 = prefab;
           // player1.GetComponentInChildren<PlayerController>().playerNum = 1;
            GameObject p1;
            p1 = player1.GetComponentInChildren<Health>().gameObject;
            player1Health.GetComponent<UIController>().trackedObject = p1;
            player1Special.GetComponent<SpecialSliderController>().trackedObject = player1.GetComponentInChildren<PlayerAbility>();
            GameController.Instance.AddPlayer(playerNum - 1, player1);
           //player1.GetComponentInChildren<Health>().takeDamage(2);
        }
        else if (newPlayerNum == 2)
        {
            player2 = prefab;
            //player2.GetComponentInChildren<PlayerController>().playerNum = 2;
            GameObject p2;
            p2 = player2.GetComponentInChildren<Health>().gameObject;
            //  p2.GetComponentInChildren<Health>().takeDamage(2);
            player2Health.GetComponent<UIController>().trackedObject = p2;
            player2Special.GetComponent<SpecialSliderController>().trackedObject = player2.GetComponentInChildren<PlayerAbility>();
            GameController.Instance.AddPlayer(playerNum - 1, player2);
        }
        else if (newPlayerNum == 3)
        {
            player3 = prefab;
          //  player3.GetComponentInChildren<PlayerController>().playerNum = 3;
            GameObject p3;
            p3 = player3.GetComponentInChildren<Health>().gameObject;
         //   p3.GetComponentInChildren<Health>().takeDamage(2);
            player3Health.GetComponent<UIController>().trackedObject = p3;
            player3Special.GetComponent<SpecialSliderController>().trackedObject = player3.GetComponentInChildren<PlayerAbility>();
            GameController.Instance.AddPlayer(playerNum - 1, player3);
        }
        else if (playerNum == 4)
        {
            player4 = prefab;
            player4.GetComponentInChildren<PlayerController>().playerNum = 4;
            GameObject p4;
            p4 = player4.GetComponentInChildren<Health>().gameObject;
           // p4.GetComponentInChildren<Health>().takeDamage(2);
            player4Health.GetComponent<UIController>().trackedObject = p4;
            player4Special.GetComponent<SpecialSliderController>().trackedObject = player4.GetComponentInChildren<PlayerAbility>();
            GameController.Instance.AddPlayer(playerNum - 1, player4);
        }
    }

    public void DespawnPlayer(int playerNum)
    {
        if (player1 && playerNum == 1)
        {
            Destroy(player1);
        }
        else if (player2 && playerNum == 2)
        {
            Destroy(player2);
        }
        else if (player3 && playerNum == 3)
        {
            Destroy(player3);
        }
        else if (player4)
        {
            Destroy(player4);
        }
    }

    public void SpawnAllPlayers()
    {
        //Debug.Log("SPAWN All");
       // theCanvas.enabled = true;
        for (int i = 0; i < GameController.Instance.Players.Length; ++i)
        {
            SpawnPlayer(i + 1);
            GameController.Instance.Players[i].GetComponent<Health>().setInvincible(7f);
        }
    }

    private GameObject GetPrefab(int index)
    {
        GameObject spawnLocation = new GameObject();
        spawnLocation.transform.position = GetSpawnLoc(index);
        if (index == 0)
        {
            return Instantiate(shieldPrefab, spawnLocation.transform.position, Quaternion.identity);
        }
        else if (index == 1)
        {
            return Instantiate(beamPrefab, spawnLocation.transform.position, Quaternion.identity);
        }
        else if (index == 2)
        {
            return Instantiate(speedPrefab, spawnLocation.transform.position, Quaternion.identity);
        }
        else if (index == -1)
        {
            return Instantiate(basePlayerPrefab, spawnLocation.transform.position, Quaternion.identity);
        }
        else return Instantiate(bubbleHealPrefab, spawnLocation.transform.position, Quaternion.identity);
    }

    public IEnumerator GG()
    {
        playersHaveSpawned = false;
        shouldSpawnPlayers = true;
        canvasSet = false;
        NewGame();
        yield return new WaitForSeconds(2f);
        for (int i = 0; i < GameController.Instance.Players.Length; ++i)
        {
            DespawnPlayer(i + 1);
        }
        // Debug.Log("
    }


    public void GameOver()
    {
      //  Debug.Log("IN gameover");
       // selections = new int[]{ -1, -1, -1, -1 };
       // theCanvas.gameObject.SetActive(false);
        playersHaveSpawned = false;
        shouldSpawnPlayers = true;
        canvasSet = false;
        NewGame();
       // Debug.Log("post bools");
        for (int i = 0; i < GameController.Instance.Players.Length; ++i)
        {
            DespawnPlayer(i + 1);
        }
       // StartCoroutine(GG());
       // Debug.Log("POST DESPAWN");
      //  NewGame();
       // Debug.Log("POST NEW GAME");
    }

    public void NewGame()
    {
      //  Debug.Log("New game called");
        gameOver = false;
        playersHaveSpawned = false;
      // theCanvas.gameObject.SetActive(true);
        canvasSet = false;
      //  Debug.Log("inactive");
        plutoWin.SetActive(false);
        plutoNext.SetActive(false);
     //   Debug.Log("set inactive");
    }

    public Vector3 GetSpawnLoc(int index = -1)
    {
        GameObject spawnLocation = new GameObject();
        if (SceneManager.GetActiveScene().name == menuSceneName)
        {
            if (index == 3)
            {
                Vector3 pos = MultiPlayerRespawner.instance.tutorialRespawnPoints[MultiPlayerRespawner.instance.respawnLoc].transform.position + Vector3.left * 5;
                return pos;
            }
            else if (index == 2)
            {
                Vector3 pos = MultiPlayerRespawner.instance.tutorialRespawnPoints[MultiPlayerRespawner.instance.respawnLoc].transform.position + Vector3.right * 5;
                return pos;
            }
            else if (index == 0)
            {
                Vector3 pos = MultiPlayerRespawner.instance.tutorialRespawnPoints[MultiPlayerRespawner.instance.respawnLoc].transform.position + Vector3.forward * 5;
                return pos;
            }
            return MultiPlayerRespawner.instance.tutorialRespawnPoints[MultiPlayerRespawner.instance.respawnLoc].transform.position;
        }
        else if (SceneManager.GetActiveScene().name == firstLevelName)
        {
            if (index == 3)
            {
                Vector3 pos = level1SpawnLoc.transform.position + Vector3.left * 5;
                return pos;
            }
            else if (index == 2)
            {
                Vector3 pos = level1SpawnLoc.transform.position + Vector3.right * 5;
                return pos;
            }
            else if (index == 0)
            {
                Vector3 pos = level1SpawnLoc.transform.position + Vector3.forward * 5;
                return pos;
            }
            return level1SpawnLoc.transform.position;
        }
        else if (SceneManager.GetActiveScene().name == secondLevelName)
        {
            if (index == 3)
            {
                Vector3 pos = level2SpawnLoc.transform.position + Vector3.left * 5;
                return pos;
            }
            else if (index == 2)
            {
                Vector3 pos = level2SpawnLoc.transform.position + Vector3.right * 5;
                return pos;
            }
            else if (index == 0)
            {
                Vector3 pos = level2SpawnLoc.transform.position + Vector3.forward * 5;
                return pos;
            }
            return level2SpawnLoc.transform.position;
        }
        else
        {
            Debug.Log("scene names in characterselector are wrong");
            return Vector3.zero;
        }
    }

    public void SwitchPlayer(int playerSelection, Vector3 position, int playerNum)
    {
        GameObject player;
        player = GetPrefab(playerSelection);
        player.transform.position = position;
        player.GetComponentInChildren<PlayerController>().playerNum = playerNum;
        GameObject p;
        p = player.GetComponentInChildren<Health>().gameObject;
        if (playerNum == 1)
        {
            player1Health.GetComponent<UIController>().trackedObject = p;
            player1Special.GetComponent<SpecialSliderController>().trackedObject = player.GetComponentInChildren<PlayerAbility>();
            player1 = player;
        }
        else if (playerNum == 2)
        {
            player2Health.GetComponent<UIController>().trackedObject = p;
            player2Special.GetComponent<SpecialSliderController>().trackedObject = player.GetComponentInChildren<PlayerAbility>();
            player2 = player;
        }
        else if (playerNum == 3)
        {
            player3Health.GetComponent<UIController>().trackedObject = p;
            player3Special.GetComponent<SpecialSliderController>().trackedObject = player.GetComponentInChildren<PlayerAbility>();
            player2 = player;
        }
        else if (playerNum == 4)
        {
            player4Health.GetComponent<UIController>().trackedObject = p;
            player4Special.GetComponent<SpecialSliderController>().trackedObject = player.GetComponentInChildren<PlayerAbility>();
            player2 = player;
        }
        GameController.Instance.AddPlayer(playerNum - 1, player);
        selections[playerNum - 1] = playerSelection;
    }

    public void PlutoWin()
    {
        plutoWin.SetActive(true);
        plutoNext.SetActive(true);
    }

    void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        if (!gameObject)
        {
            return;
        }
      //  Debug.Log("On scene load");
        playersHaveSpawned = false;
        StartCoroutine(TransitionOut());
        if(SceneManager.GetActiveScene().name == "TitleScreen")
        {
            Destroy(gameObject);
        }
    }

     public IEnumerator TransitionIn(string sceneToLoad)
    {
       Debug.Log("Transition in");
        float duration_ease_sec = 3f;
        float timer = 0;
        float progress = 0.0f;
        float timeSlow = .001f;
        Time.timeScale = timeSlow;
        while (progress < 1.0f)
        {
            progress = timer / (duration_ease_sec);
            float eased_progress = animIn.Evaluate(progress);
            transitioner.position = Vector3.LerpUnclamped(hiddenPos, visible, eased_progress);
            timer += Time.unscaledDeltaTime;
            yield return null;
        }
        if (sceneToLoad == menuSceneName)
        {
            tutorialLoad.SetActive(true);
            yield return new WaitForSeconds(timeToFill * timeSlow);
            tutorialLoad.SetActive(false);
        }
        else if (sceneToLoad == firstLevelName)
        {
            plutoLoad.SetActive(true);
            yield return new WaitForSeconds(timeToFill * timeSlow);
            plutoLoad.SetActive(false);
        }
        else if (sceneToLoad == secondLevelName)
        {
            marsLoad.SetActive(true);
            yield return new WaitForSeconds(timeToFill * timeSlow);
            marsLoad.SetActive(false);
        }
        Debug.Log("LOADING SCENE");
        SceneManager.LoadScene(sceneToLoad);
        Time.timeScale = 1;
    }

    private IEnumerator TransitionOut()
    {
        Debug.Log("TRANSITION OUT");
        float duration_ease_sec = 3f;
        float timer = 0;
        float progress = 0.0f;
        float timeSlow = .001f;
        Time.timeScale = timeSlow;
        while (progress < 1.0f)
        {
            progress = timer / (duration_ease_sec);
            float eased_progress = animOut.Evaluate(progress);
            //transitioner.
            transitioner.anchoredPosition = Vector3.LerpUnclamped(hiddenPos, visible, 1.0f - eased_progress);
            timer += Time.unscaledDeltaTime;
            yield return null;
        }
        Time.timeScale = 1;
    }
}
