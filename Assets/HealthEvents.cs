﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthEvents : MonoBehaviour
{
    [Serializable]
    public class HealthEvent
    {
        public float startingHealthPercentage;
        public float timer;
        public GameObject Event;

        public bool fired;
    }

    public HealthEvent[] Events;

    private float timer = 0;

    private GameObject currentEvent;

    // Update is called once per frame
    void Update()
    {
        if(timer > 0)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                currentEvent.active = false;
                foreach(GameObject player in GameController.Instance.Players)
                {
                    player.GetComponent<PlayerController>().DisableController = false;
                }

                if (GetComponent<miniBoss>())
                {
                    GameObject.FindGameObjectWithTag("BossMonitor").GetComponent<SkipToast>().startToasting = true;
                }
            }
        } else
        {
            foreach (HealthEvent Event in Events)
            {
                if (!Event.fired && GetComponent<Health>().getHealthPercentage() <= Event.startingHealthPercentage)
                {
                    foreach(GameObject player in GameController.Instance.Players)
                    {
                        player.GetComponent<Health>().setInvincible(Event.timer + 1);
                    }
                    currentEvent = Event.Event;
                    currentEvent.active = true;
                    Event.fired = true;
                    timer = Event.timer;
                    if (GetComponent<SpiderBody>())
                    {
                        GetComponent<SpiderBody>().forceReset(Event.timer + 1);
                        AudioManager.playSound("MarsScream",8f);
                    }
                    if (GetComponent<BurrowMovement>())
                    {
                        GetComponent<BurrowMovement>().Pause(Event.timer + 1);
                        AudioManager.playSound("PlutoScream",8f);
                    }
                    foreach (GameObject player in GameController.Instance.Players)
                    {
                        player.GetComponent<PlayerController>().DisableController = true;
                    }
                }
            }
        }
    }
}
