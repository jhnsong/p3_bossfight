﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

[RequireComponent(typeof(BossHealthBasedToastCaller))]
public class SkipToast : MonoBehaviour
{
    int curNumber = -1;
    public Sprite aBright;
    public Sprite bBright;
    public Sprite rtBright;
    public Sprite ltBright;
    public Sprite lbBright;
    public GameObject teleporter;
    private BossHealthBasedToastCaller toaster;
    public Health boss;
    public bool selectionMade = false;
    public bool skipTutorial = false;
    List<string> playerSelections;
    private List<Gamepad> gamepads;
    public GameObject normalSkip;
    public GameObject tutorialSkip;
    public GameObject a1;
    public GameObject a2;
    public GameObject a3;
    public GameObject a1Dim;
    public GameObject a2Dim;
    public GameObject a3Dim;
    public GameObject b1;
    public GameObject b2;
    public GameObject b3;
    public GameObject rt1;
    public GameObject rt2;
    public GameObject rt3;
    public GameObject rt1Dim;
    public GameObject rt2Dim;
    public GameObject rt3Dim;
    public GameObject lt1;
    public GameObject lt2;
    public GameObject lt3;
    public GameObject lt1Dim;
    public GameObject lt2Dim;
    public GameObject lt3Dim;
    public GameObject lb1;
    public GameObject lb2;
    public GameObject lb3;
    public GameObject lb1Dim;
    public GameObject lb2Dim;
    public GameObject lb3Dim;
    public bool startToasting = false;
    bool firstToasted = false;
    Color greyedColor;
    Color normalColor;
    // Start is called before the first frame update
    void Start()
    {
        greyedColor = a1Dim.GetComponent<Image>().color;
        normalColor = a1.GetComponent<Image>().color;
        gamepads = new List<Gamepad>(Gamepad.all);
        playerSelections = new List<string>();
        InitList();
        toaster = GetComponent<BossHealthBasedToastCaller>();
        boss = GameObject.FindGameObjectWithTag("Boss").GetComponent<Health>();
        //ToastManager.ToastLong("Press A to do tutorial \n Press B to skip tutorial \n (must be unanimous)");
 
    }

    // Update is called once per frame
    void Update()
    {
        if (!startToasting) return;
        if (startToasting && !firstToasted)
        {
            ToastManager.ToastLong("Press A to do tutorial \n Press B to skip tutorial \n (must be unanimous)");
            firstToasted = true;
        }

        GetInput();
        if ( startToasting && !selectionMade && toaster.GetIndex() == 0) { 
            if (CheckAllPlayersPressButton("A"))
            {
                AllPressedA();
            }
            else if (CheckAllPlayersPressButton("B"))
            {
                AllPressedB();
            }
            if (selectionMade)
            {
                if (skipTutorial)
                {
                    boss.takeDamage(5000);
                }
                else
                {
                    toaster.startToast = true;
                }
            }
        }
        else if (toaster.GetIndex() == 1)
        {
            if (curNumber != 1)
            {
                SetImgs(1);
                curNumber = 1;
            }
            if (CheckAllPlayersPressButton("RT"))
            {
                AllPressedA();
            }
        }
        else if (toaster.GetIndex() == 5)
        {
            if (curNumber != 5)
            {
                SetImgs(5);
                curNumber = 5;
            }
            if (CheckAllPlayersPressButton("LT"))
            {
                AllPressedA();
            }
        }
        else if (toaster.GetIndex() == 6)
        {
            if (curNumber != 6)
            {
                SetImgs(6);
                curNumber = 6;
            }
            if (CheckAllPlayersPressButton("LB"))
            {
                AllPressedA();
            }
        }
        else
        {
            if (curNumber != toaster.GetIndex())
            {
                SetImgs(toaster.GetIndex());
                curNumber = toaster.GetIndex();
            }
            if (CheckAllPlayersPressButton("A"))
            {
                AllPressedA();
            }
        }
    }

    private bool CheckAllPlayersPressA()
    {
        int countA = 0;
        for (int i = 0; i < playerSelections.Count; ++i)
        {
            if (playerSelections[i] == "A") countA += 1;
        }
        return countA == GameController.Instance.Players.Length;
    }

    private bool CheckAllPlayersPressB()
    {
        int countB = 0;
        for (int i = 0; i < playerSelections.Count; ++i)
        {
            if (playerSelections[i] == "B") countB += 1;
        }
        return countB == GameController.Instance.Players.Length;
    }

    private bool CheckAllPlayersPressButton(string buttonString)
    {
        int count = 0;
        for (int i = 0; i < playerSelections.Count; ++i)
        {
            if (playerSelections[i] == buttonString) count += 1;
        }
        return count == GameController.Instance.Players.Length;
    }

    private void GetInput()
    {
        for (int i = 0; i < gamepads.Count; ++i)
        {
            if (gamepads[i] == null) continue;
            //only happens in the first skip tutorial toast anyway
            if (gamepads[i].bButton.wasPressedThisFrame)
            {
                if (!selectionMade)
                {
                    playerSelections[i] = "B";
                    if (i == 0) b1.SetActive(true);
                    if (i == 1) b2.SetActive(true);
                    if (i == 2) b3.SetActive(true);
                }
                if (i == 0) a1.SetActive(false);
                if (i == 1) a2.SetActive(false);
                if (i == 2) a3.SetActive(false);
            }
            else if (toaster.GetIndex() == 5)
            {
                if (!gamepads[i].leftTrigger.wasPressedThisFrame) continue;
                playerSelections[i] = "LT";
                if (i == 0) lt1.SetActive(true);
                if (i == 1) lt2.SetActive(true);
                if (i == 2) lt3.SetActive(true);
                if (!selectionMade)
                {
                    if (i == 0) b1.SetActive(false);
                    if (i == 1) b2.SetActive(false);
                    if (i == 2) b3.SetActive(false);
                }
            }
            else if (toaster.GetIndex() == 1)
            {
                if (!gamepads[i].rightTrigger.wasPressedThisFrame) continue;
                playerSelections[i] = "RT";
                if (i == 0) rt1.SetActive(true);
                if (i == 1) rt2.SetActive(true);
                if (i == 2) rt3.SetActive(true);
                if (!selectionMade)
                {
                    if (i == 0) b1.SetActive(false);
                    if (i == 1) b2.SetActive(false);
                    if (i == 2) b3.SetActive(false);
                }
            }
            else if (toaster.GetIndex() == 6)
            { 
                if (!gamepads[i].leftShoulder.wasPressedThisFrame) continue;
                playerSelections[i] = "LB";
                if (i == 0) lb1.SetActive(true);
                if (i == 1) lb2.SetActive(true);
                if (i == 2) lb3.SetActive(true);
                if (!selectionMade)
                {
                    if (i == 0) b1.SetActive(false);
                    if (i == 1) b2.SetActive(false);
                    if (i == 2) b3.SetActive(false);
                }
            }
            else if (gamepads[i].aButton.wasPressedThisFrame)
            {
                playerSelections[i] = "A";
                if (i == 0) a1.SetActive(true);
                if (i == 1) a2.SetActive(true);
                if (i == 2) a3.SetActive(true);
                if (!selectionMade)
                {
                    if (i == 0) b1.SetActive(false);
                    if (i == 1) b2.SetActive(false);
                    if (i == 2) b3.SetActive(false);
                }
            }

        }
    }

    private void InitList()
    {
        for (int i = 0; i < gamepads.Count; ++i)
        {
            playerSelections.Add("");
        }
    }

    private void AllPressedA()
    {
        if (!selectionMade)
        {
            skipTutorial = false;
            selectionMade = true;
            tutorialSkip.SetActive(false);
        //    normalSkip.SetActive(true);
            a1Dim.SetActive(true);
            a2Dim.SetActive(true);
            a3Dim.SetActive(true);
        }
        ToastManager.instance.skipCurrentToast = true;
        ResetList();
    }

    private void AllPressedB()
    {
        if (!selectionMade)
        {
            teleporter.SetActive(false);
            skipTutorial = true;
            selectionMade = true;
            tutorialSkip.SetActive(false);
        //    normalSkip.SetActive(true);
            a1Dim.SetActive(true);
            a2Dim.SetActive(true);
            a3Dim.SetActive(true);
        }
        ToastManager.instance.skipCurrentToast = true;
        ResetList();
    }

    private void ResetList()
    {
        for (int i = 0; i < playerSelections.Count; ++i)
        {
            playerSelections[i] = "";
        }
        a1.SetActive(false);
        a2.SetActive(false);
        a3.SetActive(false);
        b1.SetActive(false);
        b2.SetActive(false);
        b3.SetActive(false);
    }

    void SetImgs(int index)
    {
        DisableAll();
        Debug.Log("SETTING SPRITE WITH INDEX " + index.ToString());
        Sprite assignSprite = aBright;
        if (index == 1)
        {
            rt1Dim.SetActive(true);
            rt2Dim.SetActive(true);
            rt3Dim.SetActive(true);
            assignSprite = rtBright;
        }
        else if (index == 5)
        {
            lt1Dim.SetActive(true);
            lt2Dim.SetActive(true);
            lt3Dim.SetActive(true);
            assignSprite = ltBright;
        }
        else if (index == 6)
        {
            lb1Dim.SetActive(true);
            lb2Dim.SetActive(true);
            lb3Dim.SetActive(true);
            assignSprite = lbBright;
        }
        else
        {
            a1Dim.SetActive(true);
            a2Dim.SetActive(true);
            a3Dim.SetActive(true);
        }
        if (!selectionMade) assignSprite = aBright;
       /* a1.GetComponent<Image>().sprite = assignSprite;
        a2.GetComponent<Image>().sprite = assignSprite;
        a3.GetComponent<Image>().sprite = assignSprite;
        a1Dim.GetComponent<Image>().sprite = assignSprite;
        a2Dim.GetComponent<Image>().sprite = assignSprite;
        a3Dim.GetComponent<Image>().sprite = assignSprite;*/


    }

    void DisableAll()
    {
        a1.SetActive(false);
        a2.SetActive(false);
        a3.SetActive(false);
        a1Dim.SetActive(false);
        a2Dim.SetActive(false);
        a3Dim.SetActive(false);
        b1.SetActive(false);
        b2.SetActive(false);
        b3.SetActive(false);
        rt1.SetActive(false);
        rt2.SetActive(false);
        rt3.SetActive(false);
        rt1Dim.SetActive(false);
        rt2Dim.SetActive(false);
        rt3Dim.SetActive(false);
        lt1.SetActive(false);
        lt2.SetActive(false);
        lt3.SetActive(false);
        lt1Dim.SetActive(false);
        lt2Dim.SetActive(false);
        lt3Dim.SetActive(false);
        lb1.SetActive(false);
        lb2.SetActive(false);
        lb3.SetActive(false);
        lb1Dim.SetActive(false);
        lb2Dim.SetActive(false);
        lb3Dim.SetActive(false);
    }



}
