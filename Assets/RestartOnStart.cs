﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartOnStart : MonoBehaviour
{
    public bool loadMenu = false;
    public bool loadFirstLevel = false;
    public bool loadSecondLevel = false;
    public bool loadGameOver = false;
    bool loadingLvl = false;
    public string sceneToLoad = "";
    public MenuInput buttons;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (loadingLvl) return;
        if ( false && (buttons.PressedA() || Input.GetKeyDown(KeyCode.KeypadEnter)))
        {
            if (loadMenu)
            {
                MultiPlayerRespawner.instance.respawnLoc = 0;
                StartCoroutine(CharacterSelector.instance.TransitionIn(CharacterSelector.instance.menuSceneName));
                ResetLvl();
                //SceneManager.LoadScene(CharacterSelector.instance.menuSceneName);
            }
            else if (loadFirstLevel)
            {
                StartCoroutine(CharacterSelector.instance.TransitionIn(CharacterSelector.instance.firstLevelName));
                ResetLvl();
                //SceneManager.LoadScene(CharacterSelector.instance.firstLevelName);
            }
            else if (loadSecondLevel)
            {
                StartCoroutine(CharacterSelector.instance.TransitionIn(CharacterSelector.instance.secondLevelName));
                ResetLvl();
                //SceneManager.LoadScene(CharacterSelector.instance.secondLevelName);
            }
            else if (loadGameOver)
            {
                StartCoroutine(CharacterSelector.instance.TransitionIn(CharacterSelector.instance.gameoverSceneName));
                ResetLvl();
                // SceneManager.LoadScene(CharacterSelector.instance.gameoverSceneName);
            }
            else
            {
                StartCoroutine(CharacterSelector.instance.TransitionIn(sceneToLoad));
                ResetLvl();
                //SceneManager.LoadScene(sceneToLoad);
            }
           
        }
        if (buttons.PressedB())
        {
            if (loadMenu)
            {
                //MultiPlayerRespawner.instance.respawnLoc = MultiPlayerRespawner.instance.tutorialRespawnPoints.Length - 1;
                CharacterSelector.instance.NewGame();
                StartCoroutine(CharacterSelector.instance.TransitionIn(GameController.Instance.loadedLevel));
                ResetLvl();
                //SceneManager.LoadScene(GameController.Instance.loadedLevel);
            }

        }
        if (buttons.PressedX())
        {
            StartCoroutine(CharacterSelector.instance.TransitionIn("TitleScreen"));
            ResetLvl();
        }
    }

    void ResetLvl()
    {
        loadingLvl = true;
        Debug.Log("loadinglvl true");
        StartCoroutine(ResetLoading());
    }
    
    IEnumerator ResetLoading()
    {
        yield return new WaitForSeconds(5f);
       // loadingLvl = false;
       // Debug.Log("its false");
    }
}
