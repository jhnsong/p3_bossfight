﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnBossNoHealth : MonoBehaviour
{
    public GameObject boss;

    // Update is called once per frame
    void Update()
    {
        if (!boss || boss.GetComponent<Health>().getHealth() <= 1)
        {
            gameObject.active = false;
        }
    }
}
